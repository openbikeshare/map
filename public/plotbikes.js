//  functions for fetch and plot bikes
// setup map
var markers = {};
var map = L.map('mapid', {
zoomDelta: 0.25,
zoomSnap: 0
  }).setView([52.0, 4.7], 11);
      L.tileLayer('https://api.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoic3ZlbjRhbGwiLCJhIjoiY2pndHI1Zjk5MTRleTJxczM1cXp0dDFmYyJ9.q2gfHAYo0g84ltS2pzWJ-Q', {
          attribution: '<a href="https://www.mapbox.com/about/maps/">Mapbox</a>; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>;<strong><a href="https://gitlab.com/bikedashboard/dashboard/wikis/home">Deelfiets Dashboard</a></strong> ',
          maxZoom: 18
      }).addTo(map);
      L.control.locate().addTo(map);

      urls = {};
      urls["ovfiets"] = "https://www.ns.nl/deur-tot-deur/ov-fiets";
      urls["flickbike"] = "https://www.flickbike.nl/";
      urls["cykl"] = "https://www.cykl.nl/";
      urls["nu-connect"] = "http://nu-connectutrecht.nl/";
      urls["nextbike"] = "https://www.nextbike.nl/en/";
      urls["nextbike"] = "https://www.nextbike.nl/en/";
      urls["donkey"] = "https://www.donkey.bike/";



// functies voor mbike
function getAndPlotBikes(options) {
  fetch(options.fetchUrl+'/bikes', { headers: {"Authorization" : options.tok }})
    .then(function (response) {
      console.log("na de fetch")
      console.log(response)
      if (response.status == 401) {
        console.log("geen toegang")
        errorNoPermission(response);
      }
      return response.json();
    }).then(function(json) {
      plotBikes(json,options);
    });
}

function plotBikes(json,options) {
  for(bike_i in json) {
    bikeInfo = json[bike_i];
    marker = L.circleMarker([bikeInfo["lat"], bikeInfo ["lon"]], markerStyle(bikeInfo,options)).addTo(map);
    marker.bindPopup(markerText(bikeInfo,options));
  }
}

function plotOBikes(json,options) {
    for(bike_i in json) {
        bikeInfo = json[bike_i];
        if (!(bikeInfo["id"] in markers)) {
          bikeInfo["bike_id"]=bikeInfo["id"]
          bikeInfo["lat"]=bikeInfo["latitude"]
          bikeInfo["lon"]=bikeInfo["longitude"]

          marker = L.circleMarker([bikeInfo["lat"], bikeInfo ["lon"]], markerStyle(bikeInfo,options)).addTo(map);
          marker.bindPopup(markerText(bikeInfo,options));
          markers[bikeInfo["bike_id"]] = marker;
        }
    }
}


function markerText(bikeInfo,options) {
  mbikeId = bikeInfo['bike_id'];
  var text = "<h2>" + mbikeId + "</h2><br>";
  // <a href='bikedetails.html?bike-id=" + mbikeId + "'>" + mbikeId + "</a>
  if (options.marker == "TimeHere1") {
    hourhere = bikeInfo["noridehour"]
    if (hourhere < 48) { text += "staat hier "+ Math.round (hourhere)+ " uur";}
     else              { text += "staat hier "+ Math.round (hourhere/24)+ " dagen";}
    text += '<br> NoReport: '   + bikeInfo["noreporthour"] +"<br>Area: "+bikeInfo["area"]
    text += "<br>battery:"+ bikeInfo['battery_voltage'] +" mVolt<br> Active status: " + bikeInfo['active status'] +"<br>Status: "+ bikeInfo['status']
    text += "<br> last report time: " + bikeInfo['last_location_report']
  }
  else // openbike
    { text += "<a href='" + urls[bikeInfo["systemId"]] + "'>" + urls[bikeInfo["systemId"]] + "</a>"
}
  return text
}

function markerStyle (bikeInfo,options) {
    var style_icon = {
      radius: 5,
      fillOpacity: 0.6
      };
  if (options.marker == "TimeHere1") {
    if (bikeInfo["noreporthour"] > 4) { style_icon["radius"] = 15 }
      difference =bikeInfo["noridehour"]
      if (difference == null) { style_icon["color"] = "black";  }
      else if (difference < 24  ) { style_icon["color"] = "green";}
      else if (difference < 2 * 24) { style_icon["color"] = "yellow";}
      else if (difference < 3 * 24) {  style_icon["color"] = "orange";}
      else if (difference < 5 * 24) { style_icon["color"] = "purple";}
      else if (difference < 7 * 24 ) { style_icon["color"] = "blue";}
      else if (difference < 14 * 24 ) {  style_icon["color"] = "red";}
      else {  style_icon["color"] = "maroon";}
    }
   else {
     if (bikeInfo["systemId"] == "donkey") {style_icon["color"] = "Chocolate"}
     else if (bikeInfo["systemId"] == "ovfiets") {style_icon["color"] = "Gold"}
     else if (bikeInfo["systemId"] == "flickbike") {style_icon["color"] = "Red"}
     else if (bikeInfo["systemId"] == "cykl") {style_icon["color"] = "Green"}
     else if (bikeInfo["systemId"] == "nextbike_bike") {style_icon["color"] = "Blue"}
     else if (bikeInfo["systemId"] == "nu-connect") {style_icon["color"] = "Teal"}
     else    {style_icon["color"] = "Black";}
    }
    return style_icon;
}
